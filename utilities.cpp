// utilities.cpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-25 11:13:33 (jonah)>

// This is part of the mpi-wavetoy-3d package. It implements a few
// small useful utility functions.
// ----------------------------------------------------------------------


// Includes
// ----------------------------------------------------------------------
#include "globals.hpp"
#include "utilities.hpp"
#include <cassert>
#include <mpi.h>
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int mod(int a, int b) {
  if (b == 0) return 0;
  if (b < 0) return mod(-a,-b);
  int ret = a % b;
  while (ret < 0) ret += b;
  return ret;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
bool is_a_cube(int n) {
  int i = 0;
  int i_cubed = i*i*i;
  while ( i_cubed <= n ) {
    if (i_cubed == n) return true;
    i += 1;
    i_cubed = i*i*i;
  }
  return false;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
bool we_integrate(int iteration, double time) {
  if ( OUTPUT_TIMING && iteration >= FINAL_ITERATION ) return false;
  if ( !(OUTPUT_TIMING) && time >= T_FINAL ) return false;
  return true;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int map_motion_to_side(int coordinate_axis, int direction) {
  assert ( 0 <= coordinate_axis && coordinate_axis <= NUM_DIMENSIONS );
  assert ( direction == FORWARD || direction == BACKWARD );
  switch (coordinate_axis) {
  case X:
    if ( direction == FORWARD ) return EAST;
    else return WEST; // BACKWARD
  case Y:
    if ( direction == FORWARD ) return NORTH;
    else return SOUTH; // BACKWARD
  default: // case z
    if ( direction == FORWARD ) return UP;
    else return DOWN; // BACKWARD
  }
}
// ----------------------------------------------------------------------
