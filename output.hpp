// output.hpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-13 22:48:50 (jonah)>

// This is part of the mpi-wavetoy-3d package. It prototypes output methods.
// ----------------------------------------------------------------------

#pragma once

// Includes
// ----------------------------------------------------------------------
#include <string>
#include <iostream>
#include <fstream>
#include "tostring.hpp"
#include "globals.hpp"
#include "data_structures.hpp"
using std::ofstream;
using std::string;
// ----------------------------------------------------------------------

/*
  OUTPUT MODEL:

  MPI rank 0 outputs error data and timing data.

  Error data comes in two forms. A final piece of error information at
  the end of the calculation and, if requested, a list of errors in U
  and RHO as a function of time.

  If more data is to be output, each rank outputs the data for a
  grid. The format is a set of rows, each row specifying the state at
  a given time and of the form:
  
  time flattened-u-data flattened-rho-data

  where the flattened data is the 3d data for a grid (excluding
  ghosts) that one traverses by raster-scanning through the grid.

  Rank 0 also outputs a "meta" file that contains all the information
  required to stitch the grid together and generate all of the data.
*/

// File name data data
// ----------------------------------------------------------------------

// Close out an output data stream. Works for any stream.
void finish_output(ofstream& outfile);

// Given a stream object initialized for us, open it with the
// appropriate filename.
// This one is for the error stream
void setup_error_stream(ofstream& outfile);

// Define the meta file that contains all the required metadata for
// the simulation. Requires the size of the mpi run, the lattice
// spacing, and the number of cells.
void setup_meta_file(int size, double h,
		     const int num_cells_1d[LOCAL_AND_GLOBAL],
		     const int dimensions[NUM_DIMENSIONS]);

// Open up the u and rho output streams and prepare them to output
// data. Requires the mpi coordinates of this rank.
void setup_u_rho_streams(ofstream& ustream, ofstream& rhostream,
			 const int mpi_coords[NUM_DIMENSIONS]);

// Print data to the u and rho files
void print_u_rho_data(const Grid& g, ofstream& ustream, ofstream& rhostream,
		      double time);

// Print this iteration's data to a stream
void save_iteration_error(ofstream& outfile,double time,
			  double u_error, double rho_error);
