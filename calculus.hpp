// calculus.hpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-18 18:43:21 (jonah)>

// This is a header file for the mpi-wavetoy-3d package. Contains
// function prototypes for differentiation routines, PDE
// right-hand-side calculaitons, and Runge-Kutta integration.
// ----------------------------------------------------------------------

#pragma once


// Includes
// ----------------------------------------------------------------------
#include <mpi.h>
#include "globals.hpp"
#include "data_structures.hpp"
// ----------------------------------------------------------------------


// Finite differences routines
// ----------------------------------------------------------------------

// Takes the second partial derivative in space, of a grid function in
// direction. Requires a grid g, the grid function "variable," the
// (processor local) index in the grid, and the lattice spacing
// h. Always performs centered differences.
double d2x(const Grid& g, int variable, int direction,
	   int ix, int iy, int iz, double h);

// Calculates the change in t per integration time step given a grid
// spacing
double get_delta_t(double h);

// ----------------------------------------------------------------------


// The wave-equation right-hand sides. Calculates du/dt and d\rho/dt at
// a given point on the grid.
// ----------------------------------------------------------------------

// Calculates the right hand side of the PDE for a given variable at a
// given position on the grid g given by (processor-local) index
// (ix,iy,iz), and at a given time. Also requires the grid spacing.
double rhs(const Grid& g, int type, int ix, int iy, int iz, double h);

// Like rhs above, but fills the new grid at index (ix,iy,iz) with the
// rhs given by the old grid.
void rhs(const Grid& old_grid, Grid& new_grid, int type,
	 int ix, int iy, int iz, double h);

// Like above, but fills in new_grid completely for all types. Uses
// MPI communication to fill in the ghost points for old_grid and then
// generates new_grid. new_grid is the same length as old_grid but
// ghost points aren't specified.
// All grids assumed to be acceptable sizes.
// Uses a set of send and receive buffers 
// Returns an MPI error code. 
int rhs(Grid& old_grid, Grid& new_grid, MPI_Comm comm,
	CommBuffer send_buffers[NUM_SIDES],
	CommBuffer recv_buffers[NUM_SIDES],
	const int neighbor_ranks[NUM_SIDES],
	MPI_Request requests[NUM_SEND_AND_RECVS],
	MPI_Status statuses[NUM_SEND_AND_RECVS],
	double h);

// ----------------------------------------------------------------------



// The Runge-Kutta integration scheme
// ----------------------------------------------------------------------

// Update the time
double new_time(double time, double h);

// An RK2 integrator that integrates the processor-local grid in time
// using the above right-hand-side. The time variable is updated
// in-place. Requires the number of local grid points and grid spacing
// h. Takes information from old_grid and fills
// integrated_grid. Performs MPI communication, so mpi information is
// required.

// Returns an error code. 
// Requires the intermediate grids k1,k1point5,k2.
// MPI requirements:
// - rank
// - size
// - the linear indices of the local grid that we send from:
//   (local_send_index)
// - the linear indices of the local grid that we receive into:
//   (local_recv_index)
// - An array of request tags
// - An array of status tags to fill
int rk2(Grid& old_grid, Grid& integrated_grid,
	Grid& k1, Grid& k1point5, Grid& k2,
	MPI_Comm comm,
	CommBuffer send_buffers[NUM_SIDES],
	CommBuffer recv_buffers[NUM_SIDES],
	const int neighbor_ranks[NUM_SIDES],
	MPI_Request requests[NUM_SEND_AND_RECVS],
	MPI_Status statuses[NUM_SEND_AND_RECVS],
	double& time, double h);

// ----------------------------------------------------------------------
