// output.cpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-15 18:56:48 (jonah)>

// This is part of the mpi-wavetoy-3d package. It implements output
// methods.
// ----------------------------------------------------------------------


// Includes
// ----------------------------------------------------------------------
#include "output.hpp"
using std::cout;
using std::endl;
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void finish_output(ofstream& outfile) {
  outfile.close();
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void setup_error_stream(ofstream& outfile) {
  outfile.open(&ERROR_FILE_NAME[0]);
  outfile << "# mpi-wavetoy error data\n"
	  << "# time\tu error\trho error"
	  << endl;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void setup_meta_file(int size, double h,
		     const int num_cells_1d[LOCAL_AND_GLOBAL],
		     const int dimensions[NUM_DIMENSIONS]) {
  ofstream metastream;
  
  // Comment lines
  metastream.open(&META_FILE_NAME[0]);
  metastream << "# mpi-wavetoy metadata\n"
	     << "# First row: "
	     << "global_xmin global_xmax "
	     << "local_num_cells global_num_cells lattice_spacing\n"
	     << "# Remaining rows: <u file names>  <rho file names> "
	     << "<mpi coordinates of those files>"
	     << endl;
  // First line
  metastream << GLOBAL_DOMAIN_MIN << " " << GLOBAL_DOMAIN_MAX << " "
	     << num_cells_1d[LOCAL] << " " << num_cells_1d[GLOBAL] << " "
	     << h
	     << endl;
  // Remaining lines defining file names
  for (int rx = 0; rx < dimensions[X]; rx++) {
    for (int ry = 0; ry < dimensions[Y]; ry++) {
      for (int rz = 0; rz < dimensions[Z]; rz++) {
	metastream << FILE_PREFACTOR << "-U-rank-"
		   << rx << "-" << ry << "-" << rz
		   << FILE_POSTFACTOR
		   << " "
		   << FILE_PREFACTOR << "-RHO-rank-"
		   << rx << "-" << ry << "-" << rz
		   << FILE_POSTFACTOR
		   << " "
		   << "(" << rx << "," << ry << "," << rz << ")"
		   << endl;
      }
    }
  }
  metastream.close();
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void setup_u_rho_streams(ofstream& ustream, ofstream& rhostream,
			 const int mpi_coords[NUM_DIMENSIONS]) {
  string u_file_name;
  string rho_file_name;
  u_file_name = FILE_PREFACTOR + "-U-rank-"
    + to_string(mpi_coords[X]) + "-" + to_string(mpi_coords[Y])
    + "-" + to_string(mpi_coords[Z]) + FILE_POSTFACTOR;
  rho_file_name = FILE_PREFACTOR + "-RHO-rank-"
    + to_string(mpi_coords[X]) + "-" + to_string(mpi_coords[Y])
    + "-" + to_string(mpi_coords[Z]) + FILE_POSTFACTOR;
  ustream.open(&u_file_name[0]);
  rhostream.open(&rho_file_name[0]);
  ustream << "# mpi-wavetoy-3d u data\n"
	  << "# time u(x,t)"
	  << endl;
  rhostream << "# mpi-wavetoy-3d rho data\n"
	    << "# time rho(x,t)"
	    << endl;
  if (DEBUGGING) {
    cout << "U file name = " << u_file_name << endl;
    cout << "RHO file name = " << rho_file_name << endl;
  }
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void print_u_rho_data(const Grid& g, ofstream& ustream, ofstream& rhostream,
		      double time) {
  ustream << time << " ";
  rhostream << time << " ";
  int imin = NUM_GHOST_CELLS;
  int imax = g.num_cells() + NUM_GHOST_CELLS;
  for (int ix = imin; ix < imax; ix++) {
    for (int iy = imin; iy < imax; iy++) {
      for (int iz = imin; iz < imax; iz++) {
	ustream << g.get_element(U,ix,iy,iz) << " ";
	rhostream << g.get_element(RHO,ix,iy,iz) << " ";
      }
    }
  }
  ustream << endl;
  rhostream << endl;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void save_iteration_error(ofstream& outfile,double time,
			  double u_error, double rho_error) {
  outfile << time << "\t" << u_error << "\t" << rho_error << endl;
}
// ----------------------------------------------------------------------
