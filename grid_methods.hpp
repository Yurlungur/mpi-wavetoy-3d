// grid_methods.hpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-13 17:29:03 (jonah)>

// This is a header file for the mpi-wavetoy-3d package. Contains
// function prototypes for methods that act on or calculate properties
// relevant to the grid on which functions evolve.
// ----------------------------------------------------------------------

#pragma once

// Includes
// ----------------------------------------------------------------------
#include "globals.hpp"
#include "data_structures.hpp"
// ----------------------------------------------------------------------


// Extracts the lattice spacing h based on the global number of cells.
double get_lattice_spacing(int global_num_cells);

// Given a local number of grid points in one direction, a local grid
// index in one direction (x,y, or z), and an mpi comm coordinate in
// the x,y, or z direction, calculate the global index of the element
// at the local index. The local index includes ghost points. The global
// one does not.  The global index should be periodic, so we need the
// global number of points in one direction too.
int get_global_index_1d(int local_index,
			const int num_cells_1d[LOCAL_AND_GLOBAL],
			int mpi_coord);

// Given the lattice spacing h and a global index in one direction
// (ix, iy, or iz), returns the projection of that position onto the
// direction axis.
double get_position_1d(int global_index, double h);

// As above, but requires local index, local and global num cells, and
//  mpi_coord instead of a global index.
double get_position_1d(int local_index,
		       const int num_cells_1d[LOCAL_AND_GLOBAL],
		       int mpi_coord);

// As above, but sets an input dVector to the three-vector
// containing the positions corresponding to the local indices
// ix,iy,iz. Requires the MPI coordinates which are accepted as a
// length-three array of integers.
void get_position_vector(int ix, int iy, int iz,
			 const int num_cells_1d[LOCAL_AND_GLOBAL],
			 const int mpi_coords[NUM_DIMENSIONS],
			 dVector& positions);

