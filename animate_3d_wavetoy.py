#!/usr/bin/env python2

"""
animate_3d_wavetoy.py

Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2014-08-25 21:18:00 (jonah)>

This is a companion program to the 3d wavetoy. It takes data output
from many files and stitches it together into a single file.

Then the program makes an animation of the wavetoy in time. The
animation is a 1-dimensional cut of the entire simulation. But you can
select along which dimension the cut should be and an offset based on
the local number of grid cells.

By default, the cut is along the x-direction starting at grid cell (0,0,0)

Example call:
python2 animate_3d_wavetoy.py mpi-wavetoy-3d100pts.meta.dat
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import sys
from optparse import OptionParser
# ----------------------------------------------------------------------


def make_3d(data,num_points):
    """
    Takes a 2d array of data (rows = times, columns = values) and
    rearranges it so that the values are arranged in a 3d array.
    """
    return np.array(map(lambda row: row.reshape((num_points,
                                                 num_points,num_points)),
                        data))


def read_data(filename):
    """
    Takes a filename for a meta file and extracts all useful
    information from it. Returns a tuple of the form:
    (positions,times,run_size,xmin,xmax,num_points,h,data_map)

    where data_map is a dictionary of the form
    {(ix,iy,iz) : (u_data, rho_data)}

    where (ix,iy,iz) are the MPI coordinates of the processor that
    generated u_data and rho_data.
    """
    with open(filename,'r') as f:
        metadata = f.readlines() # load file
    # remove comments and endline characters
    metadata = map(lambda x: x.rstrip(),
                   filter(lambda x: x[0] != "#",metadata))
    # extract metadata
    xmin,xmax,local_num_points,global_num_points,h = \
        map(lambda x: eval(x),metadata[0].split(' '))
    # Make an array of positions
    positions = np.array([xmin+h*i for i in xrange(global_num_points)])
    num_points = local_num_points
    # Extract time and u and rho data
    data_map = {}
    for line in metadata[1:]:
        columns = line.split(' ')
        u_data = np.loadtxt(columns[0],skiprows=2,ndmin=2)
        rho_data = np.loadtxt(columns[1],skiprows=2,ndmin=2)
        times = u_data[...,0]
        u_data = make_3d(u_data[...,1:],num_points)
        rho_data = make_3d(rho_data[...,1:],num_points)
        coordinates=eval(columns[2])
        data_map[coordinates]=(u_data,rho_data)
    return positions,times,xmin,xmax,local_num_points,global_num_points,h,data_map


def stitch_data(data_map,local_num_points,global_num_points):
    """
    Takes the dictionary defined in read_data and stitches the mpi
    files together so that the entire thing makes 3d grid.
    """
    if len(data_map.values()) == 1:
        u_data = data_map[(0,0,0)][0]
        rho_data = data_map[(0,0,0)][1]
        return u_data,rho_data
    
    # extract the overall shape of the aray
    num_times = len(data_map[(0,0,0)][0])
    i_max = [max(map(lambda x: x[i],data_map.keys()))+1 for i in xrange(3)]
    print i_max
    # Stack the x components
    u_x_columns = np.empty((i_max[1],i_max[2],
                            num_times,
                            global_num_points,
                            local_num_points,local_num_points))
    rho_x_columns = np.empty((i_max[1],i_max[2],
                              num_times,
                              global_num_points,
                              local_num_points,local_num_points))
    for iy in xrange(i_max[1]):
        for iz in xrange(i_max[2]):
            for t in xrange(num_times):
                u_x_columns[iy,iz,t] = np.concatenate([data_map[(ix,iy,iz)][0][t] for ix in xrange(i_max[0])],axis=0)
                rho_x_columns[iy,iz,t] = np.concatenate([data_map[(ix,iy,iz)][1][t] for ix in xrange(i_max[0])],axis=0)
    # Stack the y components
    u_xy_leaves = np.empty((i_max[2],num_times,
                            global_num_points,global_num_points,
                            local_num_points))
    rho_xy_leaves = np.empty((i_max[2],num_times,
                              global_num_points,global_num_points,
                              local_num_points))
    for iz in xrange(i_max[2]):
        for t in xrange(num_times):
            u_xy_leaves[iz,t] = np.concatenate([u_x_columns[iy,t] for iy in xrange(i_max[1])], axis=1)
            rho_xy_leaves[iz,t] = np.concatenate([rho_x_columns[iy,t] for iy in xrange(i_max[1])],axis=1)
    # stack the z components
    u_data = np.empty((times,
                       global_num_points,global_num_points,
                       global_num_points))
    rho_data = np.empty((times,
                         global_num_points,global_num_points,
                         global_num_points))
    u_data[t] = np.concatenate([u_xy_leaves[iz,t] for iz in xrange(i_max[2])],
                            axis=2)
    rho_data[t] = np.concatenate([rho_xy_leaves[iz,t] for iz in xrange(i_max[2])],axis=2)
    return u_data,rho_data
    


def load_data(filename):
    """
    Takes a filename for a meta file as a string and extracts the meta
    data from it. Uses this metadata to extract the simulation data
    from the other files.

    Returns a tuple:
    (times,positions,u_data,rho_data,run_size,h)

    times is an array of the times the simulation ran at.

    positions is an array of the positions on the grid along one
    direction. A 3d grid of positions could be generated with a
    command like np.meshgrid.

    u_data and rho_data are four-dimensional arrays. The first index
    determines the time step and the remaining three indices
    determines the grid point the data is at.

    run_size is the number of MPI ranks

    h is the lattice spacing.
    """
    positions,times,run_size,xmin,xmax,num_points,h,data_map = read_data(filename)
    u_data,rho_data = stitch_data(data_map)
    return times,positions,u_data,rho_data,run_size,h


