// globals.hpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-09-03 22:16:14 (jonah)>

// This is one of the header files for the mpi-wavetoy-3d
// package. Contains global constants.
// ----------------------------------------------------------------------

# pragma once


// Includes
// ----------------------------------------------------------------------
#include "tostring.hpp"
#include <string>
#include <cmath>
using std::sqrt;
using std::pow;
using std::string;
// ----------------------------------------------------------------------


// Basics
// ----------------------------------------------------------------------
const int NUM_VAR_TYPES = 2; // u and rho = du/dt
const int NUM_DIMENSIONS = 3; // x,y,z
const int NUM_SIDES = 2*NUM_DIMENSIONS; // A cube has six sides
const bool OUTPUT_DATA = false;   // For data if you want to plot. Only
                                  // use this to test correctness.
const bool OUTPUT_ERR = false;    // Simulates a single reduce operation.
                                  // A standard reduce for output.
const bool OUTPUT_TIMING = true;  // For timing and profiling

const bool OUTPUT_STATUS = false; // For status reports

// output debugging information. Best used with only one MPI rank.
const bool DEBUGGING = false;      

// If true, sets the local number of points in 1d (including ghosts) to
// MIN_CELLS_PER_PROC_1D. Otherwise tries to distribute
// TARGET_GLOBAL_NUM_CELLS_1D over all processors.
const bool TEST_FOR_WEAK_SCALING = false;



// Some convenient names
// ----------------------------------------------------------------------
const int U = 0;      // Variable u
const int RHO = 1;    // Variable rho
const int X = 0;      // Coordinate direction x
const int Y = 1;      // Coordinate direction y
const int Z = 2;      // Coordinate direction z

// Topologically, we're in a three torus. For convenience, we define
// directions using compas directions. We define East as the +x
// direction, north as the +y direction, and up as the +z direction.

const int WEST = 0;   // The processor to our left
const int EAST = 1;   // The processor to our right
const int SOUTH = 2;  // The processor in front of us
const int NORTH = 3;  // The processor behind us
const int DOWN = 4;     // The processor above us
const int UP = 5;   // The processor below us

const int FORWARD = 1;
const int BACKWARD = -1;
const int LOCAL = 0;
const int GLOBAL = 1;
const int LOCAL_AND_GLOBAL = 2;
const int WITHOUT_GHOSTS = 0;
const int WITH_GHOSTS = 1;



// Physics
// ----------------------------------------------------------------------

// The speed of propogation, c^2. 
const double C2 = 1;

// Minimum of domain along a given coordinate. For simplicty, the same
// in each direction.
const double GLOBAL_DOMAIN_MIN = 0.;
// Maximum of domain along a given coordinate. For simplicty, the same
// in each direction.
const double GLOBAL_DOMAIN_MAX = 2*M_PI;
// The length of the domain in each coordinate. For simplicity, the
// same in each direction.
const double L = GLOBAL_DOMAIN_MAX- GLOBAL_DOMAIN_MIN;

const double CFL_FACTOR = 0.1; // dt = CFL_FACTOR*dx

// Wave parameters. Used only for setting initial data.

// The wave number k. The number of full wavelengths that fit into one
// period in a given coordinate direction.
const double WAVE_NUMBER [] = {2.,2.,2.};
// The angular frequency of the wave in a given direction.
const double OMEGA = sqrt((WAVE_NUMBER[0]*WAVE_NUMBER[0]
			   + WAVE_NUMBER[1]*WAVE_NUMBER[1]
			   + WAVE_NUMBER[2]*WAVE_NUMBER[2])/C2);
// The amplitude of the wave in a given direction.
const double AMPLITUDE = 1;

const double T0 = 0.0; // Initial time



// Termination conditions.
// ----------------------------------------------------------------------
// If we are outputing timing but not
// outputting other data, we terminate evolution based on
// iteration. Otherwise we terminate based on time.
const double T_FINAL = 4.0;        // If we terminate integration based on
			           // time, we stop at T_FINAL.
const double FINAL_ITERATION = 160; // If we terminate integration
				   // based on iteration, this is the
				   // iteration we terminate at.


// Grid information
// ----------------------------------------------------------------------

// The number of ghost cells added at each end of the domain in a
// given direction.

// Example: If NUM_DIMENSIONS = 2 and the number of physical grid
// points on a given processor is 3 in each dimension, we have a 3x3
// grid:
//
//   O O O
//   O O O
//   O O O
//
// But if NUM_GHOST_POINTS = 1, then the grid actually looks like
// this, where physical points are big "O"s and ghost points are
// little "o"s.
// x o o o x
// o O O O o
// o O O O o
// o O O O o
// x o o o x
// The corners are marked with xs because although we allocate memory
// for them, they are not used. Removing them is considered an
// optimization
const int NUM_GHOST_CELLS = 1;


// The minimum number of physical cells along one dimensions needed
// for a single MPI rank. This ensures that every boundary cell has a
// corresponding ghost cell on a neighbor processor. We demand a 3x3x3
// grid plus ghost cells.
const int MIN_CELLS_PER_PROC_1D = 1+2*NUM_GHOST_CELLS;
// Total number of cells required. Not used.
const int MIN_CELLS_PER_PROCESSOR =
  pow(1+2*NUM_GHOST_CELLS,NUM_DIMENSIONS) // The grid without ghosts
  + 2*NUM_DIMENSIONS*pow(1+2*NUM_GHOST_CELLS, // And add the ghost cells
			 NUM_DIMENSIONS-1);


// The target number of grid points used in a given direction. For
// simplicity, all grid directions are assumed to be the same. This is
// a target number, not hte actual number. We demand that the number
// of grid points on each MPI rank be the same. This means that the
// total number of grid points might be a little larger than the
// target number.
// This parameter is only used if the variable OUTPUT_TIMING =
// false. Otherwise we demand that each MPI rank have
// MIN_CELLS_PER_PROC_1D in 1d and then the total number of grid points
// is MIN_CELLS_PER_PROC_1D^33 * <the size of the MPI run>.
const int TARGET_GLOBAL_NUM_CELLS_1D = 20;



// Integrator data. Constants needed for the Runge-Kutta integrator.
// ----------------------------------------------------------------------
const double INTERMEDIATE_WEIGHT = 1.0/2.0;
const double K1_WEIGHT = 0.0;
const double K2_WEIGHT = 1.0;



// Some MPI stuff
// ----------------------------------------------------------------------
// One send and one receive for each side of the cube.
const int NUM_SEND_AND_RECVS = 2*(2*NUM_DIMENSIONS);
// Direction tags for information flow. Directions are (in order):
// WEST, EAST, NORTH, SOUTH, UP, DOWN
const int TAGS [] = {0,1,2,3,4,5};
const int PRINT_RANK = 0; // The rank for printing
const int REORDER = 1; // Can we reorder ranks?
// Every direction in our MPI cartesion topology should be periodic
// but we have to explicitly set this. That's what this array is for
const int DIM_PERIODIC [] = {1,1,1};
// Size of every reduction operation
const int REDUCTION_SIZE = 1;


// Outputs
// ----------------------------------------------------------------------
const string POINTS_STRING = to_string(TARGET_GLOBAL_NUM_CELLS_1D);
const string FILE_PREFACTOR = "mpi-wavetoy-3d" + POINTS_STRING + "pts";
const string FILE_POSTFACTOR = ".dat";
const string META_POSTFACTOR = ".meta" + FILE_POSTFACTOR;
const string META_FILE_NAME = FILE_PREFACTOR + META_POSTFACTOR;
// Contains reduced norm2 error data
const string ERROR_FILE_NAME = FILE_PREFACTOR + ".error" + FILE_POSTFACTOR;
