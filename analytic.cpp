// mpi-wavetoy.cpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-26 11:48:58 (jonah)>

// This is part of the mpi-wavetoy-3d package. Implements methods for
// analytic solutions, initial data, and error calculations.
// ----------------------------------------------------------------------

// Includes
#include "globals.hpp"
#include "data_structures.hpp"
#include "grid_methods.hpp"
#include "analytic.hpp"
#include <iostream>
#include <cassert>
#include <mpi.h>
using std::sqrt;
using std::cos;
using std::sin;
using std::cout;
using std::endl;
// ----------------------------------------------------------------------

/*
  u = A sin(kx*x + ky*y + kz*z - omega*t)
  rho = du/dt = -A*omega*cos(kx*x + ky*x + kz*z - omega*t)
 */
// ----------------------------------------------------------------------
double analytic(int type, double t, double x, double y, double z) {
  assert (type == U || type == RHO);
  if (type == U) {
    return AMPLITUDE*sin(WAVE_NUMBER[X]*x
			 + WAVE_NUMBER[Y]*y
			 + WAVE_NUMBER[Z]*z
			 - OMEGA*t);
  } else { // if (type == RHO)
    return -OMEGA*AMPLITUDE*cos(WAVE_NUMBER[X]*x
				+ WAVE_NUMBER[Y]*y
				+ WAVE_NUMBER[Z]*z
				- OMEGA*t);
  }
}
double analytic(int type, double t, const dVector& positions) {
  return analytic(type,t,positions[X],positions[Y],positions[Z]);
}
double initial_data(int type, double x, double y, double z) {
  return analytic(type,0,x,y,z);
}
double initial_data(int type, const dVector& positions) {
  return analytic(type,0,positions);
}

// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void set_initial_data(Grid& g,const int num_cells_1d[LOCAL_AND_GLOBAL],
		      const int mpi_coords[NUM_DIMENSIONS]) {
  dVector positions(NUM_DIMENSIONS);
  for (int ix = 0; ix < g.num_cells(WITH_GHOSTS); ix++) {
    for (int iy = 0; iy < g.num_cells(WITH_GHOSTS); iy++) {
      for (int iz = 0; iz < g.num_cells(WITH_GHOSTS); iz++) {
	get_position_vector(ix,iy,iz,num_cells_1d,mpi_coords,positions);
	for (int type = 0; type < NUM_VAR_TYPES; type++) {
	  g.set_element(type,ix,iy,iz,initial_data(type,positions));
	}
      }
    }
  }
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
double local_square_error(const Grid& g, int variable, double time,
			  int ix, int iy, int iz,
			  const int num_cells_1d[LOCAL_AND_GLOBAL],
			  const int mpi_coords[NUM_DIMENSIONS],
			  double h) {
  dVector positions(NUM_DIMENSIONS);
  get_position_vector(ix,iy,iz,num_cells_1d,mpi_coords,positions);
  double analytic_solution = analytic(variable,time,positions);
  double numerical_solution = g.get_element(variable,ix,iy,iz);
  double difference = analytic_solution - numerical_solution;
  return difference*difference;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
double local_square_sum_error(const Grid& g, int variable, double time,
			      const int num_cells_1d[LOCAL_AND_GLOBAL],
			      const int mpi_coords[NUM_DIMENSIONS],
			      double h) {
  double sum = 0;
  int imin = NUM_GHOST_CELLS;
  int imax = g.num_cells() + NUM_GHOST_CELLS;
  for (int ix = imin; ix < imax; ix++) {
    for (int iy = imin; iy < imax; iy++) {
      for (int iz = imin; iz < imax; iz++) {
	sum += local_square_error(g,variable,time,ix,iy,iz,
				  num_cells_1d,mpi_coords,h);
      }
    }
  }
  return sum;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int calculate_global_error(const Grid& g,
			   double local_error_sum[NUM_VAR_TYPES],
			   double global_norm2_error[NUM_VAR_TYPES],
			   double time,
			   const int num_cells_1d[LOCAL_AND_GLOBAL],
			   const int mpi_coords[NUM_DIMENSIONS],
			   int rank,
			   MPI_Comm communicator, double h) {
  int ierr;
  for (int type = 0; type < NUM_VAR_TYPES; type++) {
    local_error_sum[type] = local_square_sum_error(g,type,time,
						   num_cells_1d,
						   mpi_coords,h);
    global_norm2_error[type] = 0;
    ierr = MPI_Reduce(&(local_error_sum[type]),&(global_norm2_error[type]),
		      REDUCTION_SIZE,MPI_DOUBLE,MPI_SUM,PRINT_RANK,
		      communicator);
    if (rank == PRINT_RANK) {
      global_norm2_error[type]
	= sqrt(global_norm2_error[type]/(num_cells_1d[GLOBAL]*num_cells_1d[GLOBAL]*num_cells_1d[GLOBAL]));
    }
  }
  return ierr;
}
// ----------------------------------------------------------------------

