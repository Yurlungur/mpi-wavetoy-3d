// grid_methods.cpp

// This is a header file for the mpi-wavetoy-3d package. Implements
// methods that act on or calculate properties relevant to the grid on
// which functions evolve.
// ----------------------------------------------------------------------


// Includes
// ----------------------------------------------------------------------
#include "globals.hpp"
#include "data_structures.hpp"
#include "grid_methods.hpp"
#include "utilities.hpp"
#include <cassert>
#include <iostream>
using std::cout;
using std::endl;
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
double get_lattice_spacing(int global_num_cells) {
  return L/double(global_num_cells);
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int get_global_index_1d(int local_index,
			const int num_cells_1d[LOCAL_AND_GLOBAL],
			int mpi_coord) {
  return mod((mpi_coord*num_cells_1d[LOCAL]+local_index-NUM_GHOST_CELLS),
	     num_cells_1d[GLOBAL]);
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
double get_position_1d(int global_index, double h) {
  double out = GLOBAL_DOMAIN_MIN + h*global_index;
  assert ( out <= GLOBAL_DOMAIN_MAX );
  return out;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
double get_position_1d(int local_index,
		       const int num_cells_1d[LOCAL_AND_GLOBAL],
		       int mpi_coord) {
  int global_index = get_global_index_1d(local_index,num_cells_1d,mpi_coord);
  double h = get_lattice_spacing(num_cells_1d[GLOBAL]);
  return get_position_1d(global_index,h);
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void get_position_vector(int ix, int iy, int iz,
			 const int num_cells_1d[LOCAL_AND_GLOBAL],
			 const int mpi_coords[NUM_DIMENSIONS],
			 dVector& positions) {
  if ( (int)positions.size() != NUM_DIMENSIONS ) {
    positions.resize(NUM_DIMENSIONS);
  }
  positions[X]=get_position_1d(ix,num_cells_1d,mpi_coords[X]);
  positions[Y]=get_position_1d(iy,num_cells_1d,mpi_coords[Y]);
  positions[Z]=get_position_1d(iz,num_cells_1d,mpi_coords[Z]);
  /*
  if (DEBUGGING) {
    cout << "Getting position vector. ("
	 << ix << "," << iy << "," << iz
	 << ") -> (" << positions[X] << "," << positions[Y] << ","
	 << positions[Z] << ")"
	 << endl;
  }
  */
}
// ----------------------------------------------------------------------

