// mpi_methods.cpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-09-03 22:07:42 (jonah)>

// This is part of the mpi-wavetoy-3d package. It implements methods
// that encapsulate MPI.

// Includes
// ----------------------------------------------------------------------
#include "globals.hpp"
#include "data_structures.hpp"
#include "mpi_methods.hpp"
#include "utilities.hpp"
#include <cassert>
#include <iostream>
using std::cout;
using std::endl;
// ----------------------------------------------------------------------



// ----------------------------------------------------------------------
int initiate_mpi(int *argcp, char ***argvp,int& rank, int& size,
		 int my_coordinates[NUM_DIMENSIONS],
		 int neighbor_ranks[NUM_SIDES],
		 int dimensions[NUM_DIMENSIONS],
		 MPI_Comm& mpi_comm_3d) {
  int ierr; // error code to return
  iVector periodic(NUM_DIMENSIONS);
  for (int i = 0; i < NUM_DIMENSIONS; i++) {
    periodic[i] = DIM_PERIODIC[i];
    dimensions[i] = 0;
  }
  ierr = MPI_Init(argcp, argvp);  // Start MPI
  ierr = MPI_Comm_size(MPI_COMM_WORLD, &size); // find size
  assert ( is_a_cube(size)
	   && "The number of mpi processes must be a cube." );
  // Calculate how to partition that many processors into 3 dimensions
  ierr = MPI_Dims_create(size,NUM_DIMENSIONS,dimensions);
  // Make a new periodic 3d grid of processes
  ierr = MPI_Cart_create(MPI_COMM_WORLD,NUM_DIMENSIONS,dimensions,
			 &(periodic[0]),REORDER,&mpi_comm_3d);
  // Calculate the MPI rank of our process
  ierr = MPI_Comm_rank(mpi_comm_3d,&rank);
  // Calculate our coordinates
  ierr = MPI_Cart_coords(mpi_comm_3d,rank,NUM_DIMENSIONS,my_coordinates);
  // Calculate the ranks of our neighbors
  ierr = MPI_Cart_shift(mpi_comm_3d,X,1,//my_coordinates[X]
			&(neighbor_ranks[WEST]),&(neighbor_ranks[EAST]));
  ierr = MPI_Cart_shift(mpi_comm_3d,Y,1,//my_coordinates[Y],
			&(neighbor_ranks[SOUTH]),&(neighbor_ranks[NORTH]));
  ierr = MPI_Cart_shift(mpi_comm_3d,Z,1,//my_coordinates[Z],
			&(neighbor_ranks[DOWN]),&(neighbor_ranks[UP]));
  // And return the error code.
  return ierr;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void set_num_points(int num_cells_1d[LOCAL_AND_GLOBAL], int size_1d) {
  if (TEST_FOR_WEAK_SCALING) {
    num_cells_1d[LOCAL] = MIN_CELLS_PER_PROC_1D;
  } else {
    num_cells_1d[LOCAL] = TARGET_GLOBAL_NUM_CELLS_1D / size_1d;
  }
  num_cells_1d[GLOBAL] = num_cells_1d[LOCAL] * size_1d;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int send_ghosts(Grid& g, MPI_Comm comm,
		CommBuffer send_buffers[NUM_SIDES],
		CommBuffer recv_buffers[NUM_SIDES],
		const int neighbor_ranks[NUM_SIDES],
		MPI_Request requests[NUM_SEND_AND_RECVS]) {
  int ierr;
  int sending_side,receiving_side;
  int tag_index = 0;
  int request_index = 0;  
  for (int axis = 0; axis < NUM_DIMENSIONS; axis++) {
    for (int direction = BACKWARD; direction <= FORWARD;
	 direction += FORWARD- BACKWARD) {
      sending_side = map_motion_to_side(axis,direction);
      receiving_side = map_motion_to_side(axis,-1*direction);
      g.fill_comm_buffer(sending_side,send_buffers[sending_side]);
      assert ( recv_buffers[receiving_side].size()
	       == send_buffers[sending_side].size() );
      ierr = MPI_Isend(&(send_buffers[sending_side][0]),
		       send_buffers[sending_side].size(),MPI_DOUBLE,
		       neighbor_ranks[sending_side],TAGS[tag_index],
		       comm,&(requests[request_index++]));
      ierr = MPI_Irecv(&(recv_buffers[receiving_side][0]),
		       recv_buffers[receiving_side].size(),MPI_DOUBLE,
		       neighbor_ranks[receiving_side],TAGS[tag_index],
		       comm,&(requests[request_index++]));
      tag_index++;
    }
  }
  assert ( tag_index == NUM_SIDES );
  assert ( request_index == NUM_SEND_AND_RECVS );
  return ierr;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int recv_ghosts(Grid& g, MPI_Comm comm, CommBuffer recv_buffers[NUM_SIDES],
		MPI_Request requests[NUM_SEND_AND_RECVS],
		MPI_Status statuses[NUM_SEND_AND_RECVS]) {
  int ierr;
  ierr = MPI_Waitall(NUM_SEND_AND_RECVS,requests,statuses);
  for (int side = 0; side < NUM_SIDES; side++) {
    g.fill_ghost_cells(side,recv_buffers[side]);
  }
  return ierr;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
double sync_watches() {
  MPI_Barrier(MPI_COMM_WORLD);
  return MPI_Wtime();
}
// ----------------------------------------------------------------------
