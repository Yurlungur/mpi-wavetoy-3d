// data_structures.hpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-18 17:55:19 (jonah)>

// This is one of the header files for the mpi-wavetoy-3d
// package. Defines data structures.
// ----------------------------------------------------------------------

# pragma once


// Includes
// ----------------------------------------------------------------------
#include <vector>
using std::vector;
// ----------------------------------------------------------------------


// Some convenient aliases
typedef vector<double> dVector;
typedef vector<int> iVector;
typedef dVector CommBuffer;


// Container class for the 1D vector.
class Grid {
public: // constructors, destructors, and assignment operators

  // Creates an empty grid ready to be resized.
  Grid();

  // Creates an empty grid with a num_cells cells in each direction.
  Grid(int num_cells);

  // Destructor
  ~Grid();

private: // Fields and private methods
  int num_cells_1d; // local number of cells in one direction. Doesn't
		    // include ghosts.
  dVector linear_grid; // Grid in memory. With ghosts

  // Gets the total number of cells (with ghosts) based on the number
  // of cells (without ghosts) in a given direction.
  int get_total_num_cells() const;

  // Gets the total number of elements of the physical vector based on
  // the number of variables and number of cells
  int get_vector_size() const;

public: // Interface
  // Resizes a grid to have the appropriate number of cells (without
  // ghosts).
  void resize(int num_cells);
  
  // Gets the linear index into the physical grid given indexes ix,
  // iy, and iz.
  int get_linear_index(int type, int ix, int iy, int iz) const;

  // Returns the local number of cells. You can find with or without
  // ghosts.
  int num_cells(int with_or_without_ghosts) const;
  
  // Returns the local number of cells, without ghosts.
  int num_cells() const;
  
  // Returns the linear length of internal dVector. i.e., linear size
  // of the grid.
  int linear_size() const;

  // Gives the number of ghost cells on a given side.
  int num_ghosts_per_side() const;
  
  // Returns the element of type at location (ix,iy,iz) on the grid
  double get_element(int type, int ix, int iy, int iz) const;
  
  // Sets the element of type at location (ix,iy,iz) on the grid to value.
  void set_element(int type, int ix, int iy, int iz, double value);

  // Linear combination routine. Fills the current grid with k1*g1 +
  // k2*g2 where k1 and k2 are assumed to be scalars and g1 and g2 are
  // assumed to be the same size as each other and self. If self is
  // not appropriately sized, it is resized.
  void linear_combination(double k1, const Grid& g1,
			  double k2, const Grid& g2);

  // Linear combination routine. Fills the current grid with k1*g1 +
  // k2*g2 + k3*g3 where k1, k2, and k3 are assumed to be scalars and
  // g1, g2, and g3 are assumed to be the same size as each other and
  // self. If self is not appropriately sized, it is resized.
  void linear_combination(double k1, const Grid& g1,
			  double k2, const Grid& g2,
			  double k3, const Grid& g3);

    // A helper function to initialize iteration through ghost
  // cells. Useful for filling and and emptying communication
  // buffers. Initializes indexes ix,iy, and iz depending on the side
  // of the cube whose ghost cells you want to iterate through.
  void initialize_ghost_iteration(int side,
				  int& ixmin, int& ixmax,
				  int& iymin, int& iymax,
				  int& izmin, int& izmax) const;

  // Like initialize ghost iteration, but for the cells just behind
  // the ghost cells... within the domain but on the domain boundary
  void initialize_boundary_iteration(int side,
				     int& ixmin, int& ixmax,
				     int& iymin, int& iymax,
				     int& izmin, int& izmax) const;

  // Fills the CommBuffer with ghost cells from a given side of the
  // cube. The buffer is assumed to be sufficiently large. If it is
  // not, it is enlarged. Side refers to the side of the cube facing
  // the directions defined above (up, down, and the compass
  // directions).
  void fill_comm_buffer(int side, CommBuffer& buffer) const;

  // Fills the grid on side with the information in ComBuffer. Side
  // refers to the side of the cube facing the directions defined
  // above (up, down, and the compass directions).
  void fill_ghost_cells(int side, CommBuffer& buffer);
};
