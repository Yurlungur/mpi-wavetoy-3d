// utilities.hpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-13 21:46:13 (jonah)>

// This is a header file for the mpi-wavetoy-3d package. Contains
// function prototypes for small utility functions.
// ----------------------------------------------------------------------

#pragma once

// Overloads the unintuitive way modulo arithmetic is defined
int mod(int a, int b);

// Determines if n is a cubic number
bool is_a_cube(int n);

// Defines the end conditions for the main loop
bool we_integrate(int iteration, double time);

// Maps a coordinate axis (x,y,z) and a direction (forward or
// backward) to a side of the cube. Essentially maps +x to EAST, -y to
// SOUTH, etc.
int map_motion_to_side(int coordinate_axis, int direction);

