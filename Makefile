# Makefile for the 3d mpi wavetoy
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2014-08-18 18:21:04 (jonah)>

# The default compiler is the MPI compiler
CXX = mpic++

# The flags for the compiler.
TESTFLAGS = -Wall -g
PRODUCTIONFLAGS = -O3

default: mpi-wavetoy-3d

all: mpi-wavetoy-3d test

mpi-wavetoy-3d: mpi-wavetoy-3d.bin

mpi-wavetoy-3d.bin: main.o analytic.o calculus.o data_structures.o grid_methods.o mpi_methods.o output.o utilities.o 
	$(CXX) $(TESTFLAGS) -o $@ $^

test: test_driver.bin

test_driver.bin: test_driver.o calculus.o data_structures.o grid_methods.o mpi_methods.o output.o utilities.o 
	$(CXX) $(TESTFLAGS) -o $@ $^

test_driver.o: test_driver.cpp globals.hpp utilities.hpp data_structures.hpp grid_methods.hpp analytic.hpp output.hpp calculus.hpp 

main.o: main.cpp globals.hpp utilities.hpp data_structures.hpp grid_methods.hpp analytic.hpp output.hpp calculus.hpp 

analytic.o: analytic.cpp analytic.hpp globals.hpp data_structures.hpp grid_methods.hpp

calculus.o: calculus.cpp calculus.hpp mpi_methods.hpp globals.hpp data_structures.hpp

output.o: output.cpp output.hpp globals.hpp tostring.hpp data_structures.hpp

mpi_methods.o: mpi_methods.cpp mpi_methods.hpp globals.hpp data_structures.hpp utilities.hpp

grid_methods.o: grid_methods.cpp grid_methods.hpp globals.hpp data_structures.hpp utilities.hpp

data_structures.o: data_structures.cpp data_structures.hpp globals.hpp utilities.hpp

utilities.o: utilities.cpp globals.hpp utilities.hpp

.PHONY: default mpi-wavetoy-3d test all

clean:
	$(RM) main.o data_structures.o analytic.o calculus.o data_structures.o grid_methods.o mpi_methods.o output.o utilities.o tostring.o
