// test_driver.cpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-18 18:28:35 (jonah)>

// This is the test driver for the mpi-wavetoy-3d package.
// ----------------------------------------------------------------------


// Includes
// ----------------------------------------------------------------------
#include <mpi.h>
#include <iostream>
#include <fstream>
#include <algorithm>   // For std::swap. For C++11, use <utility>
#include <cassert>
#include "globals.hpp"
#include "utilities.hpp"
#include "data_structures.hpp"
#include "grid_methods.hpp"
#include "analytic.hpp"
#include "output.hpp"
#include "calculus.hpp"
#include "mpi_methods.hpp"
using std::swap;
using std::cout;
using std::endl;
using std::ofstream;
// ----------------------------------------------------------------------


int main(int argc, char **argv) {
  int ierr;

  Grid g1;
  int num_cells = 3;
  int it = 0;
  int it2 = 0;
  int rank,size;

  ierr = MPI_Init(&argc,&argv);
  ierr = MPI_Comm_size(MPI_COMM_WORLD,&size);
  ierr = MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  cout << "Testing components of the MPI wavetoy package." << endl;
  cout << "-----------------------------------------------------" << endl;

  cout << "Let's test the grid data structure." << endl;
  cout << endl;
  cout << "Initializing the structure\n"
       << "with " <<  num_cells << " cells." << endl;
  g1.resize(num_cells);
  cout << endl;
  cout << "Number of cells in the structure, including ghosts,\n"
       << "along one dimensions is: " << g1.num_cells(WITH_GHOSTS) << endl;
  cout << endl;
  cout << "Now iterating through the structure\n"
       << "and filling it with a linear index." << endl;

  for (int type = 0; type < NUM_VAR_TYPES; type++) {
    for (int ix = 0; ix < g1.num_cells(WITH_GHOSTS); ix++) {
      for (int iy = 0; iy < g1.num_cells(WITH_GHOSTS); iy++) {
	for (int iz = 0; iz < g1.num_cells(WITH_GHOSTS); iz++) {
	  g1.set_element(type, ix,iy,iz,it++);
	}
      }
    }
  }
  cout << endl;
  cout << "The contents are (iterating type,x,y,z):" << endl;
  cout << "For U" << endl;
  for (int ix = 0; ix < g1.num_cells(WITH_GHOSTS); ix++) {
      for (int iy = 0; iy < g1.num_cells(WITH_GHOSTS); iy++) {
	cout << "[ ";
	for (int iz = 0; iz < g1.num_cells(WITH_GHOSTS); iz++) {
	  cout << g1.get_element(U,ix,iy,iz) << " ";
	  it2++;
	}
	cout << "]" << endl;
      }
      cout << "\n" << endl;
    }

  cout << "For RHO" << endl;
  for (int ix = 0; ix < g1.num_cells(WITH_GHOSTS); ix++) {
      for (int iy = 0; iy < g1.num_cells(WITH_GHOSTS); iy++) {
	cout << "[ ";
	for (int iz = 0; iz < g1.num_cells(WITH_GHOSTS); iz++) {
	  cout << g1.get_element(RHO,ix,iy,iz) << " ";
	  it2++;
	}
	cout << "]" << endl;
      }
      cout << "\n" << endl;
  }
  assert (it == it2);

  MPI_Finalize();
  return 0;
}
