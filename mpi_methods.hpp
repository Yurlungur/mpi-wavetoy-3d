// mpi-methods.hpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-09-03 22:08:01 (jonah)>

// This is a header file for the mpi-wavetoy-3d package. It prototypes
// methods that encapsulate MPI.
// ----------------------------------------------------------------------

#pragma once

// Includes
// ----------------------------------------------------------------------
#include <mpi.h>
#include "globals.hpp"
#include "data_structures.hpp"
// ----------------------------------------------------------------------

// Note that, for simplicity, we demand that every grid object be a
// cube. This means that the number of mpi processors must be n^3
// where n is an integer. If this is not the case, the program will
// raise an error.

// Starts the MPI. Needs the pointers to the command line arguments
// which it reads. Returns an error code. Also needs pointers to the
// the following parameters, which will be set:

// The this processor's rank
// The size of the mpi run
// the MPI Comm for the 3d cartesian grid
// dimensions, a 1d array giving the number of mpi processes in each direction
// The coordinates of this mpi rank in the cartesian grid. (1d array)
// The ranks of the neighbors... (1d array. 6 elements one for each
// side of the cube.)
int initiate_mpi(int *argc, char ***argv,int& rank, int& size,
		 int my_coordinates[NUM_DIMENSIONS],
		 int neighbor_ranks[NUM_SIDES],
		 int dimensions[NUM_DIMENSIONS],
		 MPI_Comm& mpi_comm_3d);

// Sets the local and global number of cells in one direction
 // (without ghosts) based on the number of mpi processes in that
// direction in the cartesian grid. Fills the arryay num_cells_1d.
void set_num_points(int num_cells_1d[LOCAL_AND_GLOBAL], int size_1d);

// Uses an array of six communication buffers for sending
// (send_buffers) and six for receiving (recv_buffers) to communicate
// information about the ghost cells of a grid between MPI
// processes. Sends data up, down, and along the compass
// directions. Returns an MPI error code. Needs information about the
// grid, the mpi communicator, the list of neighbors, and an array of
// MPI_Requests to fill. Only initiates the send.
int send_ghosts(Grid& g, MPI_Comm comm,
		CommBuffer send_buffers[NUM_SIDES],
		CommBuffer recv_buffers[NUM_SIDES],
		const int neighbor_ranks[NUM_SIDES],
		MPI_Request requests[NUM_SEND_AND_RECVS]);

// After all the work that can be done asynchronously is done, looks
// for the received data, ensures it is there, and fills the ghost
// cells of the grid g. Requires a good bit of MPI data. The mpi
// communicator, the receive buffers, and arrays of requests and
// statuses for the MPI communications. Returns an error code.
int recv_ghosts(Grid& g, MPI_Comm comm, CommBuffer recv_buffers[NUM_SIDES],
		MPI_Request requests[NUM_SEND_AND_RECVS],
		MPI_Status statuses[NUM_SEND_AND_RECVS]);

// Forces all mpi processes to synchronize and returns the time post
// synchronization.
double sync_watches();
