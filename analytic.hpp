// mpi-wavetoy.hpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-13 21:58:53 (jonah)>

// This is a header file for the mpi-wavetoy-3d package. Contains
// function prototypes for analytic solutions, initial data, and error
// calculations.
// ----------------------------------------------------------------------

#pragma once


// Includes
// ----------------------------------------------------------------------
#include "globals.hpp"
#include "data_structures.hpp"
#include <mpi.h>
// ----------------------------------------------------------------------


// Initial Data and Analytic Solutions
// ----------------------------------------------------------------------
/*
  The analytic solution is a product of travelling waves that travel
  along the coordinate directions. In each direction, the wave looks like:
  u(t,x) = A * cos(k*x - omega*t)
  where A is the amplitude, k is the wavenumber, and c^2 = omega/k
 */
// Analytic U solution in one direction
double analytic_u_1d(double t, double x, double a, double k, double omega);

// Analytic RHO solution in one direction
double analytic_rho_1d(double t, double x, double a, double k, double omega);

// Analytic solution in one direction. Use type to select between U and RHO.
double analytic_1d(int type, double t, double x,
		double a, double k, double omega);

// The analytic solution in 3d. Use type to select between U and RHO
double analytic(int type, double t, double x, double y, double z);
// As above but positions given in a single length 3 dVector
double analytic(int type, double t, const dVector& positions);

// Initial data in 3d. Use type to select between U and RHO.
double initial_data(int type, double x, double y, double z);
// As above but positions given in a single length 3 dVector
double initial_data(int type, double t, const dVector& positions);

// Fills a local grid g with initial data. Requires the number of
// points both locally and globally and requires the coordinates in
// the 3d MPI communicator of the current MPI process.
void set_initial_data(Grid& g,const int num_cells_1d[LOCAL_AND_GLOBAL],
		      const int mpi_coords[NUM_DIMENSIONS]);

// ----------------------------------------------------------------------


// Error calculations
// ----------------------------------------------------------------------

// Calculates the error in a variable at an index (ix,iy,iz).
// For convenience, error is calculated as
// (variable - solution)^2.
// This reduces operations later and removes sign ambiguity.
double local_square_error(const Grid& g, int variable, double time,
			  int ix, int iy, int iz,
			  const int num_cells_1d[LOCAL_AND_GLOBAL],
			  const int mpi_coords[NUM_DIMENSIONS],
			  double h);

// Given a grid, the time, and a variable type,
// calculates the sum of the squares of all errors on the grid
double local_square_sum_error(const Grid& g, int variable, double time,
			      const int num_cells_1d[LOCAL_AND_GLOBAL],
			      const int mpi_coords[NUM_DIMENSIONS],
			      double h);

// Calculates the global norm2 error using an MPI reduce
// operation. Returns the MPI error code.
// Inputs:
//
// local_error_sum[], empty array that will be filled with the local
//                       square sums of the error for both U and RHO.
//
// global_norm2_error[], empty array that will be filled with the
//                       GLOBAL square sum of the errors accross all
//                       grids and processors.
//
// Other inputs self-explanatory
int calculate_global_error(const Grid& g,
			   double local_error_sum[NUM_VAR_TYPES],
			   double global_norm2_error[NUM_VAR_TYPES],
			   double time,
			   const int num_cells_1d[LOCAL_AND_GLOBAL],
			   const int mpi_coords[NUM_DIMENSIONS],
			   int rank,
			   MPI_Comm communicator, double h);

// ----------------------------------------------------------------------
