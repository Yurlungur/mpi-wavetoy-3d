// main.cpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-09-03 21:58:15 (jonah)>

// This is the main loop for the mpi-wavetoy-3d package.
// ----------------------------------------------------------------------


// Includes
// ----------------------------------------------------------------------
#include <mpi.h>
#include <iostream>
#include <fstream>
#include <algorithm>   // For std::swap. For C++11, use <utility>
#include "globals.hpp"
#include "utilities.hpp"
#include "data_structures.hpp"
#include "grid_methods.hpp"
#include "analytic.hpp"
#include "output.hpp"
#include "calculus.hpp"
#include "mpi_methods.hpp"
using std::swap;
using std::cout;
using std::endl;
using std::ofstream;
// ----------------------------------------------------------------------


int main(int argc, char **argv) {

  // Initialize variables
  // --------------------------------------------------
  int ierr; // error variable

  // Output
  ofstream err_file;
  ofstream u_file;
  ofstream rho_file;

  // Number of cells in one direction
  int num_cells_1d[LOCAL_AND_GLOBAL];

  // Grid for two time levels. Old and integrated grids.
  int old = 0;
  int integrated = 1;
  Grid local_grid[2];


  // Intermediate grids used for the RK2 scheme
  Grid k1;
  Grid k1point5;
  Grid k2;

  // Physics
  double h; // The grid spacing we'll eventually settle on
  double time = T0; // The time variable
  double local_error_sum[NUM_VAR_TYPES];  // Holds processor-local error

  // This variable will hold our global error, which we'll print
  // out. Only rank 0 needs this but I just initialized it everywhere.
  double global_norm2_error[NUM_VAR_TYPES];

  // MPI
  int size; // Number of MPI processes
  int rank; // Index of our process
  MPI_Comm mpi_comm_3d; // The MPI Cartesian grid
  int dimensions[NUM_DIMENSIONS];   // The shape of the 3d cartesian
				    // grid of MPI processors
  int mpi_coords[NUM_VAR_TYPES]; // 3d coordinates in MPI cartesian grid

  // Communication buffers
  CommBuffer send_buffers[NUM_SIDES];
  CommBuffer recv_buffers[NUM_SIDES];

  // Coordinate for the grids overlapping with ours on each side of
  // the cube
  int neighbor_ranks[NUM_SIDES];

  // For non-blocking sends and receives, you need a status tag and a
  // request tag. That's these. We one for each send or receive for
  // each variable.
  MPI_Status statuses[NUM_SEND_AND_RECVS];
  MPI_Request requests[NUM_SEND_AND_RECVS];

  // Time tracking
  double start,end,elapsed_time;
  int iterations = 0;
  // --------------------------------------------------

  // Initialize MPI
  ierr = initiate_mpi(&argc,&argv,rank,size,
		      mpi_coords,neighbor_ranks,dimensions,
		      mpi_comm_3d);

  if (DEBUGGING && rank == PRINT_RANK) {
    cout << "MPI initialized." << endl;
    cout << "\tNeighbor ranks = [ ";
    for (int side = 0; side < NUM_SIDES; side++) {
      cout << neighbor_ranks[side] << " ";
    }
    cout << "]" << endl;
  }

  // Calculates the number of local grid points per processor
  set_num_points(num_cells_1d,dimensions[0]);

  if (DEBUGGING) {
    cout << "Local num points = " << num_cells_1d[LOCAL] << "\n"
	 << "Global num points = " << num_cells_1d[GLOBAL]
	 << endl;
  }

  // Set up the grid
  h = get_lattice_spacing(num_cells_1d[GLOBAL]);
  if (DEBUGGING) cout << "Grid spacing = " << h << endl;

  for (int i = 0; i < 2; i++) {
    if (DEBUGGING) cout << "resizing grid " << i << endl;
    local_grid[i].resize(num_cells_1d[LOCAL]);
  }
  k1.resize(local_grid[0].num_cells());
  k1point5.resize(k1.num_cells());
  k2.resize(k1.num_cells());
  for (int side = 0; side < NUM_SIDES; side++) {
    send_buffers[side].resize(NUM_VAR_TYPES*k1.num_ghosts_per_side());
    recv_buffers[side].resize(NUM_VAR_TYPES*k1.num_ghosts_per_side());
  }

  if (DEBUGGING) cout << "Grids generated" << endl;

  // Initialize all output files
  if (OUTPUT_ERR && rank == PRINT_RANK) {
    setup_error_stream(err_file);
  }
  if (OUTPUT_DATA) {
    if (rank == PRINT_RANK) {
      setup_meta_file(size,h,num_cells_1d,dimensions);
    }
    setup_u_rho_streams(u_file,rho_file,mpi_coords);
  }

  // Set up initial data
  set_initial_data(local_grid[old],num_cells_1d,mpi_coords);

  // Start timing
  start = sync_watches();

  // Main loop
  while ( we_integrate(iterations,time) ) {
    // First print 3d data if necessary
    if (OUTPUT_DATA) print_u_rho_data(local_grid[old],u_file,rho_file,time);
    // And calculate error if necessary
    if (OUTPUT_ERR) {
      ierr = calculate_global_error(local_grid[old],
				    local_error_sum,global_norm2_error,time,
				    num_cells_1d,mpi_coords,rank,
				    mpi_comm_3d,h);
      if (rank == PRINT_RANK) {
	save_iteration_error(err_file,time,
			     global_norm2_error[U],
			     global_norm2_error[RHO]);
      }
    }
    // And integrate
    ierr = rk2(local_grid[old],local_grid[integrated],k1,k1point5,k2,
	       mpi_comm_3d,send_buffers,recv_buffers,neighbor_ranks,
	       requests,statuses,time,h);
    // our integrated grid will be our old grid for the next iteration
    swap(old,integrated);
    // we've iterated once
    iterations++;

  }
  // Stop timing
  end = sync_watches();
  elapsed_time = end - start;
  // close our output streams
  if (OUTPUT_ERR) finish_output(err_file);
  if (OUTPUT_DATA) {
    finish_output(u_file);
    finish_output(rho_file);
  }

  // Finally, if we're outputting timing but not outputting the error,
  // calculate the final error, just to make sure nothing broke.
  if ( OUTPUT_TIMING && !(OUTPUT_ERR) ) {
    ierr = calculate_global_error(local_grid[old],
				  local_error_sum,global_norm2_error,time,
				  num_cells_1d,mpi_coords,rank
				  ,mpi_comm_3d,h);
  }

  // If we're outputting timing data, send it to the console
  if (rank == PRINT_RANK && OUTPUT_TIMING) {
    cout << "global num points = " << num_cells_1d[GLOBAL] << endl;
    cout << "MPI run size = " << size << endl;
    cout << "Error data:\n"
	 << "\tU norm2 error: " << global_norm2_error[U] << "\n"
	 << "\tRHO norm2 error: " << global_norm2_error[RHO] << "\n"
	 << "Timing data:\n"
	 << "\tTotal run time: "
	 << elapsed_time << " seconds\n"
	 << "\tTotal iterations: " << iterations << "\n"
	 << "\tRK substeps per iteration: " << 2 << "\n"
	 << "\tTime per substep: "
	 << elapsed_time/(2*iterations) << " seconds.\n"
	 << "Error code = " << ierr
	 << endl;
  }

  // End MPI and return
  MPI_Finalize();
  return 0;
}
