// calculus.cpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-25 17:38:28 (jonah)>

// This is a part of the mpi-wavetoy-3d package. Implements:
// differentiation routines, PDE right-hand-side calculations, and
// Runge-Kutta integration.
// ----------------------------------------------------------------------


// Includes
// ----------------------------------------------------------------------
#include "mpi_methods.hpp"
#include "calculus.hpp"
#include <cassert>
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
double d2x(const Grid& g, int variable, int direction,
	   int ix, int iy, int iz, double h) {
  assert ( variable == U || variable == RHO );
  assert ( direction == X || direction == Y || direction == Z );
  double center_element,right_hand_element,left_hand_element,out;
  center_element = g.get_element(variable,ix,iy,iz);
  switch (direction) {
  case X:
    right_hand_element = g.get_element(variable,ix+1,iy,iz);
    left_hand_element = g.get_element(variable,ix-1,iy,iz);
    break;
  case Y:
    right_hand_element = g.get_element(variable,ix,iy+1,iz);
    left_hand_element = g.get_element(variable,ix,iy-1,iz);
    break;
  default: // case Z
    right_hand_element = g.get_element(variable,ix,iy,iz+1);
    left_hand_element = g.get_element(variable,ix,iy,iz-1);
  }
  out = (right_hand_element - 2*center_element + left_hand_element)/(h*h);
  return out;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
double get_delta_t(double h) {
  return CFL_FACTOR * h;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
double rhs(const Grid& g, int type, int ix, int iy, int iz, double h) {
  assert (type == U || type == RHO);
  double sum = 0;
  if (type == U) return g.get_element(RHO,ix,iy,iz);
  else { // If type == RHO
    for (int direction = 0; direction < NUM_DIMENSIONS; direction++) {
      sum += d2x(g,U,direction,ix,iy,iz,h);
    }
  }
  return C2*sum;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void rhs(const Grid& old_grid,Grid& new_grid, int type,
	   int ix, int iy, int iz, double h) {
  new_grid.set_element(type,ix,iy,iz,
		       rhs(old_grid,type,ix,iy,iz,h));
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int rhs(Grid& old_grid, Grid& new_grid, MPI_Comm comm,
	CommBuffer send_buffers[NUM_SIDES],
	CommBuffer recv_buffers[NUM_SIDES],
	const int neighbor_ranks[NUM_SIDES],
	MPI_Request requests[NUM_SEND_AND_RECVS],
	MPI_Status statuses[NUM_SEND_AND_RECVS],
	double h) {
  int ierr;
  int imin_inner = 2*NUM_GHOST_CELLS;
  int imax_inner = old_grid.num_cells();
  int ix,iy,iz,type;
  int ixmin_side,ixmax_side,iymin_side,iymax_side,izmin_side,izmax_side;
  ierr = send_ghosts(old_grid,comm,send_buffers,recv_buffers,
		     neighbor_ranks,requests);
  // Take the derivative where we can. If we can't, wait for MPI. This
  // fills in the subcube of size num_cells_1d - 2*NUM_GHOST_CELLS in
  // each direction.
  for (type = 0; type < NUM_VAR_TYPES; type++) {
    for (ix = imin_inner; ix < imax_inner; ix++) {
      for (iy = imin_inner; iy < imax_inner; iy++) {
	for (iz = imin_inner; iz < imax_inner; iz++) {
	  rhs(old_grid,new_grid,type,ix,iy,iz,h);
	}
      }
    }
  }
  // Now we wait
  ierr = recv_ghosts(old_grid,comm,recv_buffers,requests,statuses);
  // Integrate what's left. In other words, integrate each boundary
  // side.
  for (type = 0; type < NUM_VAR_TYPES; type++) {
    for (int side = 0; side < NUM_SIDES; side++) {
      old_grid.initialize_boundary_iteration(side,
					     ixmin_side,ixmax_side,
					     iymin_side,iymax_side,
					     izmin_side,izmax_side);
      for (ix = ixmin_side; ix < ixmax_side; ix++) {
	for (iy = iymin_side; iy < iymax_side; iy++) {
	  for (iz = izmin_side; iz < izmax_side; iz++) {
	    rhs(old_grid,new_grid,type,ix,iy,iz,h);
	  }
	}
      }
    }
  }
  return ierr;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
double new_time(double time, double h) {
  return time + get_delta_t(h);
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int rk2(Grid& old_grid, Grid& integrated_grid,
	Grid& k1, Grid& k1point5, Grid& k2,
	MPI_Comm comm,
	CommBuffer send_buffers[NUM_SIDES],
	CommBuffer recv_buffers[NUM_SIDES],
	const int neighbor_ranks[NUM_SIDES],
	MPI_Request requests[NUM_SEND_AND_RECVS],
	MPI_Status statuses[NUM_SEND_AND_RECVS],
	double& time, double h) {
  int ierr;
  double delta_t = get_delta_t(h);

  // k1 = rhs(old_grid)
  ierr = rhs(old_grid,k1,comm,send_buffers,recv_buffers,neighbor_ranks,
	     requests,statuses,h);
  // k1point5 = old+grid + INTERMEDIATE_WEIGHT*delta_t*k1
  k1point5.linear_combination(1.0,old_grid,
			      INTERMEDIATE_WEIGHT*delta_t,k1);
  // k2 = rhs(k1point5)
  ierr = rhs(k1point5,k2,comm,send_buffers,recv_buffers,neighbor_ranks,
	     requests,statuses,h);
  // integrated_grid = old_grid + delta_t*(K1_WEIGHT*k1 + K2_WEIGHT*k2)
  integrated_grid.linear_combination(1.0,old_grid,
				     delta_t*K1_WEIGHT,k1,
				     delta_t*K2_WEIGHT,k2);
  // update time
  time = new_time(time,h);
  // return
  return ierr;
}
// ----------------------------------------------------------------------
