// tostring.hpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-13 21:45:22 (jonah)>

// An implementation of the to_string() method. Already implemented in
// C++11 but if we implement it oursleves we're not dependant on that
// being available.

#pragma once
#include <sstream>
#include <string> // Should be already included in sstream
using std::string;
using std::ostringstream;

template<class TYPE>
string to_string(TYPE val) {
  ostringstream convert;
  convert << val;
  return convert.str();
}

