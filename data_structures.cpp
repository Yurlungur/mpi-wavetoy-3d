// data_structures.cpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-25 18:24:31 (jonah)>

// This is part of the mpi-wavetoy-3d package. Implements data
// structures.
// ----------------------------------------------------------------------

// Includes
// ----------------------------------------------------------------------
#include "globals.hpp"
#include "utilities.hpp"
#include "data_structures.hpp"
#include <cassert>
#include <iostream>
using std::cout;
using std::endl;
using std::pow;
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
Grid::Grid() {} // Empty
Grid::Grid(int num_cells) {
  resize(num_cells);
}
Grid::~Grid() {} // Destructor is empty. 
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void Grid::resize(int num_cells) {
  int new_size;
  num_cells_1d = num_cells;
  if (DEBUGGING) cout << "\tnum_cells_1d = " << num_cells_1d << endl;
  assert (get_total_num_cells() >= MIN_CELLS_PER_PROCESSOR
	  && "every boundary cell has a corresponding ghost");
  new_size = get_vector_size();
  if (DEBUGGING) cout << "\tnew_size = " << new_size << endl;
  linear_grid.resize(new_size);  
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int Grid::get_total_num_cells() const {
  return pow(num_cells(WITH_GHOSTS),NUM_DIMENSIONS);
}
int Grid::get_vector_size() const {
  return get_total_num_cells()*NUM_VAR_TYPES;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int Grid::get_linear_index(int type, int ix, int iy, int iz) const {
  assert (type == U || type == RHO);
  assert (0 <= ix && ix <= num_cells(WITH_GHOSTS));
  assert (0 <= iy && iy <= num_cells(WITH_GHOSTS));
  assert (0 <= iz && iz <= num_cells(WITH_GHOSTS));
  int out;
  out = (
	 type*get_total_num_cells()
	 + ix*num_cells(WITH_GHOSTS)*num_cells(WITH_GHOSTS)
	 + iy*num_cells(WITH_GHOSTS)
	 + iz
	 );
  return out;
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int Grid::num_cells() const {
  return num_cells(WITHOUT_GHOSTS);
}
int Grid::num_cells(int with_or_without_ghosts) const {
  assert (with_or_without_ghosts == WITH_GHOSTS
	  || with_or_without_ghosts == WITHOUT_GHOSTS);
  if (with_or_without_ghosts == WITH_GHOSTS) {
    return num_cells_1d + 2*NUM_GHOST_CELLS;
  } else {
    return num_cells_1d;
  }
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
int Grid::linear_size() const {
  return linear_grid.size();
}
double Grid::get_element(int type, int ix, int iy, int iz) const {
  return linear_grid[get_linear_index(type,ix,iy,iz)];
}
void Grid::set_element(int type, int ix, int iy, int iz, double value) {
  linear_grid[get_linear_index(type,ix,iy,iz)] = value;
}
int Grid::num_ghosts_per_side() const {
  return pow(num_cells_1d,NUM_DIMENSIONS-1);
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void Grid::linear_combination(double k1, const Grid& g1,
			      double k2, const Grid& g2) {
  assert ( g1.num_cells() == g2.num_cells() );
  if ( num_cells() != g1.num_cells() ) {
    resize(g1.num_cells());
  }
  int imax = num_cells(WITH_GHOSTS);
  for (int type = 0; type < NUM_VAR_TYPES; type++) {
    for (int ix = 0; ix < imax; ix++) {
      for (int iy = 0; iy < imax; iy++) {
	for (int iz = 0; iz < imax; iz++) {
	  set_element(type,ix,iy,iz,
		      k1*g1.get_element(type,ix,iy,iz)
		      +k2*g2.get_element(type,ix,iy,iz));
	}
      }
    }
  }
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void Grid::linear_combination(double k1, const Grid& g1,
			      double k2, const Grid& g2,
			      double k3, const Grid& g3) {
  assert ( g1.num_cells() == g2.num_cells()
	   && g2.num_cells() == g3.num_cells() );
  if ( num_cells() != g1.num_cells() ) {
    resize(g1.num_cells());
  }
  int imax = num_cells(WITH_GHOSTS);
  for (int type = 0; type < NUM_VAR_TYPES; type++) {
    for (int ix = 0; ix < imax; ix++) {
      for (int iy = 0; iy < imax; iy++) {
	for (int iz = 0; iz < imax; iz++) {
	  set_element(type,ix,iy,iz,
		      k1*g1.get_element(type,ix,iy,iz)
		      +k2*g2.get_element(type,ix,iy,iz)
		      +k3*g3.get_element(type,ix,iy,iz));
	}
      }
    }
  }
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void Grid::initialize_ghost_iteration(int side,
				      int& ixmin, int& ixmax,
				      int& iymin, int& iymax,
				      int& izmin, int& izmax) const {
  int generic_min = NUM_GHOST_CELLS;
  int generic_max = num_cells_1d + NUM_GHOST_CELLS;
  ixmin = generic_min;
  iymin = generic_min;
  izmin = generic_min;
  ixmax = generic_max;
  iymax = generic_max;
  izmax = generic_max;
  switch (side) {
  case WEST:
    ixmin = 0;
    ixmax = NUM_GHOST_CELLS;
    break;
  case EAST:
    ixmin = generic_max;
    ixmax = generic_max + NUM_GHOST_CELLS;
    break;
  case NORTH:
    iymin = generic_max;
    iymax = generic_max + NUM_GHOST_CELLS;
    break;
  case SOUTH:
    iymin = 0;
    iymax = NUM_GHOST_CELLS;
    break;
  case UP:
    izmin = generic_max;
    izmax = generic_max + NUM_GHOST_CELLS;
    break;
  default: //case DOWN:
    izmin = 0;
    izmax = NUM_GHOST_CELLS;
  }
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void Grid::initialize_boundary_iteration(int side,
					 int& ixmin, int& ixmax,
					 int& iymin, int& iymax,
					 int& izmin, int& izmax) const {
  int generic_min = NUM_GHOST_CELLS;
  int generic_max = num_cells_1d + NUM_GHOST_CELLS;
  ixmin = generic_min;
  iymin = generic_min;
  izmin = generic_min;
  ixmax = generic_max;
  iymax = generic_max;
  izmax = generic_max;
  switch (side) {
  case EAST:
    ixmin = generic_max - NUM_GHOST_CELLS;
    break;
  case WEST:
    ixmax = generic_min + NUM_GHOST_CELLS;
    break;
  case NORTH:
    iymin = generic_max - NUM_GHOST_CELLS;
    break;
  case SOUTH:
    iymax = generic_min + NUM_GHOST_CELLS;
    break;
  case UP:
    izmin = generic_max - NUM_GHOST_CELLS;
    break;
  default: // case DOWN
    izmax = generic_min + NUM_GHOST_CELLS;
    break;
  }
}
// ----------------------------------------------------------------------




// ----------------------------------------------------------------------
void Grid::fill_comm_buffer(int side, CommBuffer& buffer) const {
  if (DEBUGGING) {
    cout << "filling comm buffer\n"
	 << "\tSide = " << side
	 << endl;
  }
  assert (0 <= side && side < NUM_SIDES);
  int ixmin,ixmax,iymin,iymax,izmin,izmax; 
  int buffer_index = 0;
  int buffer_size = NUM_VAR_TYPES*num_ghosts_per_side();
  assert ( (int)buffer.size() == buffer_size );
  initialize_boundary_iteration(side,ixmin,ixmax,iymin,iymax,izmin,izmax);
  if (DEBUGGING) {
    cout << "\tix in [" << ixmin << ", " << ixmax << "]\n"
	 << "\tiy in [" << iymin << ", " << iymax << "]\n"
      	 << "\tiz in [" << izmin << ", " << izmax << "]\n"
	 << endl;
  }
  for (int type = 0; type < NUM_VAR_TYPES; type++) {
    if (DEBUGGING) cout << "\tType = " << type << endl;
    for (int ix = ixmin; ix < ixmax; ix++) {
      for (int iy = iymin; iy < iymax; iy++) {
	for (int iz = izmin; iz < izmax; iz++) {
	  buffer[buffer_index] = get_element(type,ix,iy,iz);
	  buffer_index++;
	}
      }
    }
  }
  if (DEBUGGING) {
    cout << "Comm filled. Final index = " << buffer_index << endl;
    cout << "The buffer's size is: " << buffer.size() << endl;
  }
  assert (buffer_index == (int)buffer.size() && "The buffer must be full.");
}
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
void Grid::fill_ghost_cells(int side, CommBuffer& buffer) {
  assert (0 <= side && side < NUM_SIDES);
  int ixmin,ixmax,iymin,iymax,izmin,izmax; 
  int buffer_index = 0;
  int buffer_size = NUM_VAR_TYPES*num_ghosts_per_side();
  assert ( (int)buffer.size() == buffer_size );
  initialize_ghost_iteration(side,ixmin,ixmax,iymin,iymax,izmin,izmax);
  for (int type = 0; type < NUM_VAR_TYPES; type++) {
    for (int ix = ixmin; ix < ixmax; ix++) {
      for (int iy = iymin; iy < iymax; iy++) {
	for (int iz = izmin; iz < izmax; iz++) {
	  set_element(type,ix,iy,iz,buffer[buffer_index]);
	  buffer_index++;
	}
      }
    }
  }
  if (DEBUGGING) {
    cout << "Comm filled. Final index = " << buffer_index << endl;
  }
  assert (buffer_index == (int)buffer.size()
	  && "We must have iterated through the whole buffer.");
}
// ----------------------------------------------------------------------
